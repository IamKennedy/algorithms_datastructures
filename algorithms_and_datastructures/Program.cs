﻿using System;

namespace algorithms_and_datastructures
{
    class Program
    {
        static void Main(string[] args)
        {
            /*String Reversal Test Cases*/
            Console.WriteLine(reverse("apple"));

            Console.WriteLine(reverse("hello"));

            Console.WriteLine(reverse("Greetings!"));

            Console.ReadLine();
        }

        public static string reverse(string stringToBeReversed)   
        {
            var watch = new System.Diagnostics.Stopwatch();

            char[] stringCharacters = stringToBeReversed.ToCharArray();

            string reversedString = "";

            int counter = 0; 

            for (int i = stringCharacters.Length -1 ; counter != stringCharacters.Length; i--) 
            {
                reversedString = reversedString + stringCharacters[i];

                counter++;
            }

            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

            return reversedString;
        }
    }
}
