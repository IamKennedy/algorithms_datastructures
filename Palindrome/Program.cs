﻿using System;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Palindrome Test Cases*/

            

            Console.WriteLine(palindrome("abba"));

            Console.WriteLine(palindrome("abcdefg"));

            Console.ReadLine();
        }

        public static string palindrome(string palindromeTestWord) 
        {
            var watch = new System.Diagnostics.Stopwatch();

            char[] stringCharacters = palindromeTestWord.ToCharArray();

            string reversedString = "";

            int counter = 0;

            for (int i = palindromeTestWord.Length - 1; counter != palindromeTestWord.Length; i--) 
            {
                reversedString = reversedString + stringCharacters[i];
                counter++;
            }

            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

            return (reversedString == palindromeTestWord).ToString();

        }
    }
}
