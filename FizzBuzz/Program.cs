﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzz(90);

            Console.ReadLine();
        }

        public static void FizzBuzz(int number)
        {
            var watch = new System.Diagnostics.Stopwatch();

            int counter = 1;

            while (!(counter > number))
            {                 
                if ((counter % 3 == 0) && (counter % 5 == 0))
                {
                    Console.WriteLine("fizzbuzz");
                }
                else if (counter % 5 == 0)
                {
                    Console.WriteLine("buzz");
                }
                else if (counter % 3 == 0)
                {
                    Console.WriteLine("fizz");
                }
                else 
                {
                    Console.WriteLine(counter);
                }
                ++counter;
            }

            Console.WriteLine($"Execution Time: {watch.Elapsed} ms");
        }
    }

    
}
