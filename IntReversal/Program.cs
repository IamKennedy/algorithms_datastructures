﻿using System;

namespace IntReversal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(reverseInt(15));
            Console.WriteLine(reverseInt(981));
            Console.WriteLine(reverseInt(500));
            Console.WriteLine(reverseInt(-15));
            Console.WriteLine(reverseInt(-90));
            Console.WriteLine(reverseInt(-900000000));

            Console.ReadLine();
        }

    

        public static string reverseInt(int number) 
        {
            var watch = new System.Diagnostics.Stopwatch();

            bool isNumberNegetive = false;

            string reversedString = "";

            if (number < 0) 
            {
                number = number * -1;

                isNumberNegetive = true;
            }

            if (number == 0) 
            {
                return number.ToString();
            }

            char[] numberChars = number.ToString().ToCharArray();

            int counter = 0;

            for (int i = numberChars.Length - 1; counter != numberChars.Length; i--)
            {
                if (numberChars[i] == '0')
                {
                    counter++;
                    continue;
                }
                else 
                {
                    reversedString = reversedString + numberChars[i];
                    counter++;
                }
            }

            if (isNumberNegetive) 
            {
                int reversedInt = Convert.ToInt32(reversedString) * -1;

                reversedString = reversedInt.ToString();

            }
            
            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

            return reversedString;

        }

    }
}
