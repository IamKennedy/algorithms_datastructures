﻿using System;
using System.Collections.Generic;

namespace MaxChars
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(MaxChars("abcccccccddddddddddddddddddddddddddddddddddiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiioooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooopppppppppppppppppppppppppppppppppppppppppppppppjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"));

            Console.WriteLine(MaxChars("apple 1231111"));

            Console.ReadLine();
        }


        public static string MaxChars(string testString) 
        {
            var watch = new System.Diagnostics.Stopwatch();

            var recordBook = new Dictionary<char, int>();

            var charCounter = 0;

            var counter = 0;

            int max = 0;

            char charWithMaxValue = '.';

            char[] testStringChars = testString.ToCharArray();

            for (int i = 0; i <= testStringChars.Length - 1; i++) 
            {
                for (int j = 0; j <= testStringChars.Length - 1; j++)
                {
                    if (testStringChars[i] == testStringChars[j]) 
                    {
                        charCounter++;
                    }              
                }

                recordBook[testStringChars[i]] = charCounter;

                charCounter = 0;

                counter++;
            }

            foreach (var record in recordBook) 
            {
                if (record.Value > max) 
                {
                    max = record.Value;

                    charWithMaxValue = record.Key;
                }
            }


            Console.WriteLine($"Execution Time: {watch.Elapsed} ms");

            return $"{charWithMaxValue} : {max}";

            
        }
    }
}
